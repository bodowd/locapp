from flask import Flask, render_template, request, url_for
from PIL import Image
import numpy as np
from io import BytesIO
import requests


app = Flask(__name__)


@app.route('/')
def home():
    r = requests.get("https://www.loc.gov/collections/?fo=json").json()
    result = {}
    while True:
        for c in r['results']:
            result[c['title']] = c['url']
        next_page = r['pagination']['next']
        if next_page is not None:
            r = requests.get(next_page).json()
        else:
            break

    return render_template('home.html', title='Home', results=result)


@app.route('/collection_images/<lnk>', methods=['GET', 'POST'])
def collection_images(lnk):
    base_url = "https://www.loc.gov/collections/"
    lnk = base_url+lnk+'/?fo=json'
    collections_json = requests.get(lnk).json()
    results = []
    for c in collections_json['results']: # list of dictionaries
        title = c['title']
        img = c['image_url']
        if img != []:
            # try to get a larger image otherwise get the smaller one
            try:
                img = img[1]
            except:
                img = img[0]
            results.append([title, img])
    return render_template('collection_images.html', title='Images from LOC',
                            results=results)

if __name__ == "__main__":
    app.run(debug=True)
