# Explore the Library of Congress collections' images

This connects to the Library of Congress API to list the collections and after selecting a collection of interest, see the images.

# How to use

`$ git clone ...`

`$ python -m venv venv`

`$ pip install requirements.txt`

`$ source venv/bin/activate`

`$ python app.py`

go to localhost:5000 in your browser

